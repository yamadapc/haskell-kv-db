{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
module Worker
  where

import Control.Distributed.Process (Process, ProcessId, expect, send)
import Control.Distributed.Process.Closure
import Data.Binary (Binary)
import GHC.Generics
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Typeable

type Key   = String
type Value = String

data Message = MessageSet Key Value
             | MessageGet ProcessId Key
  deriving(Typeable, Generic, Show)

instance Binary Message

worker :: Process ()
worker = loop Map.empty
  where
    loop m = do
       msg <- expect
       case msg of
           MessageGet i k -> send i (Map.lookup k m) >> loop m
           MessageSet k v -> loop (Map.insert k v m)

remotable ['worker]
