{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TemplateHaskell #-}
module Database (
       Database,
       Key, Value,
       createDB,
       get, set,
       rcdata,
  ) where

import Control.Distributed.Process
import Control.Distributed.Process.Closure
import Control.Monad (forM)
import Data.Binary (Binary)
import Data.Char
import GHC.Generics
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Typeable

import Worker

type Database = ProcessId

loop :: [ProcessId] -> Process ()
loop ps = do
    msg <- expect
    let Just p = slaveForKey msg
    send p msg
    loop ps
  where
    nslaves = length ps
    slaveForKey (MessageSet [] _) = Nothing
    slaveForKey (MessageGet _ []) = Nothing
    slaveForKey (MessageSet k _) = Just $ ps !! (ord (head k) `mod` nslaves)
    slaveForKey (MessageGet _ k) = Just $ ps !! (ord (head k) `mod` nslaves)


createDB :: [NodeId] -> Process Database
createDB ns = do
    ps <- forM ns $ \n -> do
        p <- spawn n $(mkStaticClosure 'worker)
        say ("Spawned node on " ++ show p)
        return p
    spawnLocal (loop ps)

set :: Database -> Key -> Value -> Process ()
set db k v = send db (MessageSet k v)

get :: Database -> Key -> Process (Maybe Value)
get db k = do
    i <- getSelfPid
    send db (MessageGet i k)
    expect

rcdata :: RemoteTable -> RemoteTable
rcdata = Worker.__remoteTable
